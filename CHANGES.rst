Changelog
=========

0.0.2 (2021-12-12)
------------------
- Package name change to spotify.dj.
- Import util from spotify.util package.

0.0.1 (2021-12-10)
------------------
- Implementation of dance playlist creator.

0.0.0 (2021-12-09)
------------------
- Initial commit.
