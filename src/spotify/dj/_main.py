import sys
import yaml
import random

from pathlib import Path

from spotify.util import get_items
from spotify.core import Spotify
from spotify.core import User
from spotify.core import Playlist

playlists_ids = [
    "7LQYhFnA1E0uDYpy72cDwG", # _Player
    "0be6EvJ9HgycKckllBLfZt", # ToSort
    "0xKrHeDSgq6DIJ9IIfjJCt", # DoPrzesłuchania
    "6d99wVqadk83dQ53ilTG4P", # Let's Rock
    "4wzv3quxG7Pvmwg9zY1vSN", # Acoustic
    "5V7wZte5L6703U8LvA3KQm", # Productive
    "3H7scue8dAmkG5OJbJCdSA", # Happy
    # "??????????????????????", # Chill
    "7IVA4dwO1IP7EdU06Mbf70", # Calm
    "3Na9ScRR292M5xaTEPHBZX", # Epic
    "08UthXFBiAvbGpNMYBFUrn", # Magic
    "0rRbHyNtDlX1xn2XvgfV09", # Nostalgic
    "4ARMqJVyYCei9jvRc3zdRY", # Love
    "70TLDjhkUMqIE4DRbaMdSe", # Pop
    "7kGdOuxtjfw2S6ekWZnCth", # Sad
    "4hX8h1QlEJwhA1Ic4rNVIq", # Shanty
    "5DrgPkYSdX6rES6MLYDJJl", # Catchy
    "1afJ0g8hlaj4JAvaGPpSLP"  # Let's burn this house down
]

def concatenate_generators(gen_list:list, sequentially=True):
    """ Gets values from multiple generators like it was one generator.

        Arguments:
            sequentially    - if True, generators will be emptied from first to last
                              if False, elements will be taken randomly from all generators
    """
    if sequentially:
        for gen in gen_list:
            try:
                while True:
                    yield next(gen)
            except StopIteration:
                pass
    else:
        while len(gen_list) > 0:
            gen_idx = random.randint(0, len(gen_list) - 1)
            try:
                yield next(gen_list[gen_idx])
            except StopIteration:
                del gen_list[gen_idx]

def load_tracks(playlists, tracks_total=50, playlists_shuffled=False):
    """ Returns generator with tracks to add. """
    tracks_gens = []
    for playlist in playlists:
        gens = []
        for sub_playlist in playlist:
            gens.append(sub_playlist.get_tracks(shuffled=True, repetition=True, cyclic=True))

        tracks_gens.append(concatenate_generators(gens, False))

    def getter(idx):
        return next(tracks_gens[idx])

    return get_items(getter, len(tracks_gens), shuffled=True, repetition=True, cyclic=True, limit=tracks_total)

def get_playlists(config_path:Path):
    with config_path.open("r+") as file:
        config = yaml.load(file, Loader=yaml.FullLoader)

    playlists = []
    dance_names = []
    for dance_name, data in config.items():
        dance_names.append(dance_name)
        sub_playlists = []
        for sub_plst_data in data["playlists"]:
            sub_playlists.append(Playlist.get_by_id(sub_plst_data["id"]))
        playlists.append(sub_playlists)
    return (playlists, dance_names)

def create_playlist(name, tracks_num, config_path:Path) -> Playlist:
    # TODO: check if playlist with such name already exist
    plst = Playlist.create_for_user(name)

    # adding tracks to new playlist
    playlists, dance_names = get_playlists(config_path)
    tracks_generator = load_tracks(playlists, tracks_num, True)
    plst.add_tracks(tracks_generator)

    # TODO: create mechanism for update Playlist when values are changed,
    #       then return plst obj instead of create new one
    return Playlist.get_by_id(plst.id)

def show_tracks_with_their_dance_names(playlist:Playlist, config_path:Path):
    playlists, dance_names = get_playlists(config_path)
    print("{:<50} {}".format("TRACK", "PLAYLISTS"))
    print("=" * 100)
    for track in playlist.get_tracks(limit=playlist.tracks_total):
        plsts = []
        for sub_playlist, dance_name in zip(playlists, dance_names):
            if sum(1 for dummy in Spotify.get_playlists_with_track(track, sub_playlist)) > 0:
                plsts.append(dance_name)
        print("{:<50} {}".format(track.name, plsts))


def main(argv=sys.argv[1:]):
    spotify = Spotify()
    config_path = Path(Path.home()/"spotify/manager/instances")/"instance1.yaml"

    plst = create_playlist("DancingDJ_2", 30, config_path)
    show_tracks_with_their_dance_names(plst, config_path)


def main_old(argv=sys.argv[1:]):
    instances_path = Path.home()/"spotify/manager/instances"
    instances_path.mkdir(parents=True, exist_ok=True)
    instances_pattern = f"{instances_path}/*.json"

    # create list of instances
    self.instances = { Path(instance).stem : Instance(Path(instance).stem, Path(instance)) \
        for instance in glob.glob(instances_pattern) }

    pm = PackageManager()
    pl = pm.create("instance1")
    pm.show_containing_playlists("instance1", pl)

    # print(User.get_by_name("1195144592"))
    # for pl in Playlist.get_by_user(User.get_current()):
    #     print(pl)

    # new_pl = Playlist.create_for_user("Jive", User.get_current())
    # pl = Playlist.get_by_id("2lBbkgYgjtQksUHqmxEvIm")
    # new_pl.add_tracks(pl.get_tracks())


# if __name__.rpartition(".")[-1] == "__main__":
#     sys.exit(main(sys.argv[1:]))
