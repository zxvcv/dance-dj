from numpy.random import choice

def randomize_get(A, num:int):
    """ """
    while num > 0:
        if num < len(self.A):
            size = num
        else:
            size = len(self.A)

        choices = choice(self.A, size=size, replace=False, p=None)
        for i in choices:
            yield i
        num -= size
